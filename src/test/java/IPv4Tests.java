import InetSubnet.InetSubnet;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;

import InetSubnet.Inet4Subnet;


public class IPv4Tests {
    private final Inet4Subnet v4 = Inet4Subnet.of("10.0.0.0/23");

    @Test
    public void testIDifyMyIdsIPv4(){
        try{
            byte[] b = {10, 0, 0, 0};
            InetAddress id = InetAddress.getByAddress(b);
            for(int i = 0; i < 512; i++){
                InetSubnet test;
                if(i < 256)
                    test = InetSubnet.of("10.0.0." + i + "/23");
                else
                    test = InetSubnet.of("10.0.1." + (i - 256) + "/23");
                assertEquals(id, test.getSubnetId());
            }
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void testGetMaskIPv4(){
        try{
            Inet4Address[] masks = {
                    (Inet4Address) InetAddress.getByName("255.0.0.0"),
                    (Inet4Address) InetAddress.getByName("255.128.0.0"),
                    (Inet4Address) InetAddress.getByName("255.192.0.0"),
                    (Inet4Address) InetAddress.getByName("255.224.0.0"),
                    (Inet4Address) InetAddress.getByName("255.240.0.0"),
                    (Inet4Address) InetAddress.getByName("255.248.0.0"),
                    (Inet4Address) InetAddress.getByName("255.252.0.0"),
                    (Inet4Address) InetAddress.getByName("255.254.0.0"),
                    (Inet4Address) InetAddress.getByName("255.255.0.0"),
                    (Inet4Address) InetAddress.getByName("255.255.128.0"),
                    (Inet4Address) InetAddress.getByName("255.255.192.0"),
                    (Inet4Address) InetAddress.getByName("255.255.224.0"),
                    (Inet4Address) InetAddress.getByName("255.255.240.0"),
                    (Inet4Address) InetAddress.getByName("255.255.248.0"),
                    (Inet4Address) InetAddress.getByName("255.255.252.0"),
                    (Inet4Address) InetAddress.getByName("255.255.254.0"),
                    (Inet4Address) InetAddress.getByName("255.255.255.0"),
                    (Inet4Address) InetAddress.getByName("255.255.255.128"),
                    (Inet4Address) InetAddress.getByName("255.255.255.192"),
                    (Inet4Address) InetAddress.getByName("255.255.255.224"),
                    (Inet4Address) InetAddress.getByName("255.255.255.240"),
                    (Inet4Address) InetAddress.getByName("255.255.255.248"),
                    (Inet4Address) InetAddress.getByName("255.255.255.252"),
                    (Inet4Address) InetAddress.getByName("255.255.255.254"),
                    (Inet4Address) InetAddress.getByName("255.255.255.255")
            };
            for(int i = 0; i < masks.length; i++){
                Inet4Subnet test = Inet4Subnet.of("10.0.0.0/" + (8 + i));
                assertEquals(masks[i],test.getMask());
            }
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void testBroadCastIPv4(){
        int i = 0;
        try{
            Inet4Address[] bcasts = {
                    (Inet4Address) InetAddress.getByName("10.255.255.255"),     // 10.0.0.0/8
                    (Inet4Address) InetAddress.getByName("10.127.255.255"),     // 10.0.0.0/9
                    (Inet4Address) InetAddress.getByName("10.63.255.255"),      // 10.0.0.0/10
                    (Inet4Address) InetAddress.getByName("10.31.255.255"),      // 10.0.0.0/11
                    (Inet4Address) InetAddress.getByName("10.15.255.255"),      // 10.0.0.0/12
                    (Inet4Address) InetAddress.getByName("10.7.255.255"),       // 10.0.0.0/13
                    (Inet4Address) InetAddress.getByName("10.3.255.255"),       // 10.0.0.0/14
                    (Inet4Address) InetAddress.getByName("10.1.255.255"),       // 10.0.0.0/15
                    (Inet4Address) InetAddress.getByName("10.0.255.255"),       // 10.0.0.0/16
                    (Inet4Address) InetAddress.getByName("10.0.127.255"),       // 10.0.0.0/17
                    (Inet4Address) InetAddress.getByName("10.0.63.255"),        // 10.0.0.0/18
                    (Inet4Address) InetAddress.getByName("10.0.31.255"),        // 10.0.0.0/19
                    (Inet4Address) InetAddress.getByName("10.0.15.255"),        // 10.0.0.0/20
                    (Inet4Address) InetAddress.getByName("10.0.7.255"),         // 10.0.0.0/21
                    (Inet4Address) InetAddress.getByName("10.0.3.255"),         // 10.0.0.0/22
                    (Inet4Address) InetAddress.getByName("10.0.1.255"),         // 10.0.0.0/23
                    (Inet4Address) InetAddress.getByName("10.0.0.255"),         // 10.0.0.0/24
                    (Inet4Address) InetAddress.getByName("10.0.0.127"),         // 10.0.0.0/25
                    (Inet4Address) InetAddress.getByName("10.0.0.63"),          // 10.0.0.0/26
                    (Inet4Address) InetAddress.getByName("10.0.0.31"),          // 10.0.0.0/27
                    (Inet4Address) InetAddress.getByName("10.0.0.15"),          // 10.0.0.0/28
                    (Inet4Address) InetAddress.getByName("10.0.0.7"),           // 10.0.0.0/29
                    (Inet4Address) InetAddress.getByName("10.0.0.3"),           // 10.0.0.0/30
                    (Inet4Address) InetAddress.getByName("10.0.0.1"),           // 10.0.0.0/31
                    (Inet4Address) InetAddress.getByName("10.0.0.0")            // 10.0.0.0/32
            };
            for(; i < bcasts.length; i++){
                Inet4Subnet test = Inet4Subnet.of("10.0.0.0/" + (8 + i));
                assertEquals(bcasts[i], test.getBroadcast());
            }
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void testContainsIPv4(){ // Testing 2 bytes worth of IP-address combinations.
        try{
            Inet4Subnet test = Inet4Subnet.of("10.0.0.0/16");
            for(int i = 0; i < 256; i++){
                for(int j = 0; j < 256; j++){
                    String ip = "10.0." + i + "." + j;
                    assertTrue(test.contains(InetAddress.getByName(ip)));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void testGetNthAddressIPv4(){ // Tests both methods of get IPv4 Nth address.
        for(int i = 0; i < 256; i++){
            try{
                String ip = "10.0.0." + i;
                assertEquals(v4.getNthAddress(BigInteger.valueOf(i)), getAddress(ip));
                assertEquals(v4.getNthAddress(BigInteger.valueOf(i)), v4.getNthAddress(i));
            }catch (AssertionError ae){
                Assert.fail();
            }
            assertEquals(v4.getNthAddress(BigInteger.valueOf(256)), getAddress("10.0.1.0"));
            assertEquals(v4.getNthAddress(BigInteger.valueOf(511)), getAddress("10.0.1.255")); // Number of addresses i 2^n - 1, hennce 511 for (512 - 1)
        }
    }

    @Test
    public void testGetFirstAddressIPv4(){ // Tests both methods of get IPv4 Nth address.
        try{
            Inet4Address target = (Inet4Address) InetAddress.getByName("10.0.0.1");
            for(int i = 8; i < 32; i++){
                Inet4Subnet test = Inet4Subnet.of("10.0.0.0/" + i);
                assertEquals(target, test.getFirstAddress());
            }
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void testGetLastAddressIPv4(){
        try{
            Inet4Address[] ips = {
                    (Inet4Address) InetAddress.getByName("10.255.255.255"),
                    (Inet4Address) InetAddress.getByName("10.127.255.255"),
                    (Inet4Address) InetAddress.getByName("10.63.255.255"),
                    (Inet4Address) InetAddress.getByName("10.31.255.255"),
                    (Inet4Address) InetAddress.getByName("10.15.255.255"),
                    (Inet4Address) InetAddress.getByName("10.7.255.255"),
                    (Inet4Address) InetAddress.getByName("10.3.255.255"),
                    (Inet4Address) InetAddress.getByName("10.1.255.255"),
                    (Inet4Address) InetAddress.getByName("10.0.255.255"),
                    (Inet4Address) InetAddress.getByName("10.0.127.255"),
                    (Inet4Address) InetAddress.getByName("10.0.63.255"),
                    (Inet4Address) InetAddress.getByName("10.0.31.255"),
                    (Inet4Address) InetAddress.getByName("10.0.15.255"),
                    (Inet4Address) InetAddress.getByName("10.0.7.255"),
                    (Inet4Address) InetAddress.getByName("10.0.3.255"),
                    (Inet4Address) InetAddress.getByName("10.0.1.255"),
                    (Inet4Address) InetAddress.getByName("10.0.0.255"),
                    (Inet4Address) InetAddress.getByName("10.0.0.127"),
                    (Inet4Address) InetAddress.getByName("10.0.0.63"),
                    (Inet4Address) InetAddress.getByName("10.0.0.31"),
                    (Inet4Address) InetAddress.getByName("10.0.0.15"),
                    (Inet4Address) InetAddress.getByName("10.0.0.7"),
                    (Inet4Address) InetAddress.getByName("10.0.0.3"),
                    (Inet4Address) InetAddress.getByName("10.0.0.1"),
                    (Inet4Address) InetAddress.getByName("10.0.0.0"),
            };
            for(int i = 8; i < 32; i++){
                Inet4Subnet test = Inet4Subnet.of("10.0.0.0/" + i);
                assertEquals(ips[i - 8], test.getLastAddress());
            }
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void testGetRandomAddress(){ // Given that function 'contains()' is 100% correct.
        try{
            for(int i = 0; i < (1 << 20); i++){
                assertTrue(v4.contains(v4.getRandomAddress()));
            }
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void testSummarizeIPv4(){
        try{

            // Both subnets are the same.

            Inet4Subnet a = Inet4Subnet.of("10.0.0.0/24");
            Inet4Subnet b = Inet4Subnet.of("10.0.0.0/24");
            assertEquals(a, InetSubnet.summarize(a,b));

            // Adjecent continous subnet summary

            a = Inet4Subnet.of("10.0.0.0/24");
            b = Inet4Subnet.of("10.0.1.0/24");
            Inet4Subnet c = Inet4Subnet.of("10.0.0.0/23");
            assertEquals(c, InetSubnet.summarize(a,b));

            // Subnet a contains entire subnet b

            a = Inet4Subnet.of("10.0.0.0/24");
            b = Inet4Subnet.of("10.0.0.128/25");
            c = Inet4Subnet.of("10.0.0.0/24");
            assertEquals(c, InetSubnet.summarize(a,b));

            a = Inet4Subnet.of("10.0.0.0/24");
            b = Inet4Subnet.of("10.0.0.64/26");
            c = Inet4Subnet.of("10.0.0.0/24");
            assertEquals(c, InetSubnet.summarize(a,b));

        }catch (Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void testSortIPv4(){
        try{
            Inet4Subnet a = Inet4Subnet.of("10.1.0.0/24");
            Inet4Subnet b = Inet4Subnet.of("10.2.0.0/24");
            Inet4Subnet c = Inet4Subnet.of("10.2.0.0/24");
            Inet4Subnet d = Inet4Subnet.of("10.3.0.0/24");
            Inet4Subnet e = Inet4Subnet.of("10.3.0.0/24");
            Inet4Subnet f = Inet4Subnet.of("10.4.0.0/24");
            Inet4Subnet g = Inet4Subnet.of("10.5.0.0/24");
            Inet4Subnet h = Inet4Subnet.of("10.6.0.0/24");
            ArrayList<Inet4Subnet> list = new ArrayList<>();
            ArrayList<Inet4Subnet> correct = new ArrayList<>();

            // Insert in arbitrary order. Not sorted.
            list.add(b);
            list.add(h);
            list.add(g);
            list.add(e);
            list.add(a);
            list.add(c);
            list.add(d);
            list.add(f);

            // Insert in correct sorted order. Lowest numerical value first.
            correct.add(a);
            correct.add(b);
            correct.add(c);
            correct.add(d);
            correct.add(e);
            correct.add(f);
            correct.add(g);
            correct.add(h);

            // Sort
            Collections.sort(list);

            // Verify
            for(int i = 0; i < list.size(); i++)
                if(!list.get(i).equals(correct.get(i)))
                    Assert.fail();

            Assert.assertTrue(true);
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    private InetAddress getAddress(String ip){
        try{
            return InetAddress.getByName(ip);
        }catch (Exception e){
            System.exit(-1);
            return null;
        }
    }
}
