import InetSubnet.Inet6Subnet;
import InetSubnet.InetSubnet;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import java.math.BigInteger;
import java.net.Inet6Address;
import java.net.InetAddress;

public class IPv6Tests {
    private Inet6Subnet v6 = Inet6Subnet.of("fd00:dead:beef:cafe::/64");

    @Test
    public void testGetMaskIPv6(){
        try{
            Inet6Address[] masks = {
                    (Inet6Address) InetAddress.getByName("f000::"),
                    (Inet6Address) InetAddress.getByName("ff00::"),
                    (Inet6Address) InetAddress.getByName("fff0::"),
                    (Inet6Address) InetAddress.getByName("ffff::"),
                    (Inet6Address) InetAddress.getByName("ffff:f000::"),
                    (Inet6Address) InetAddress.getByName("ffff:ff00::"),
                    (Inet6Address) InetAddress.getByName("ffff:fff0::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:f000::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ff00::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:fff0::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:f000::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:ff00::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:fff0::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:ffff::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:ffff:f000::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:ffff:ff00::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:ffff:fff0::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:ffff:ffff::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:ffff:ffff:f000::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:ffff:ffff:ff00::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:ffff:ffff:fff0::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:ffff:ffff:ffff::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:ffff:ffff:ffff:f000::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:ffff:ffff:ffff:ff00::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:ffff:ffff:ffff:fff0::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:ffff:ffff:ffff:ffff::"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:ffff:ffff:ffff:ffff:f000"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ff00"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:ffff:ffff:ffff:ffff:fff0"),
                    (Inet6Address) InetAddress.getByName("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff")
            };
            for(int i = 8; i < masks.length; i++){
                Inet6Subnet test = Inet6Subnet.of("fd00::/" + ((i + 1) * 4));
                assertEquals(test.getMask(), masks[i]);
            }
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void testContainsIPv6(){ // Testing 2 bytes worth of IP-address combinations.
        try{
            for(int i = 0; i < (1 << 16); i++){
                String ip = "fd00:dead:beef:cafe::" + Integer.toHexString(i);
                assertTrue(v6.contains(InetAddress.getByName(ip)));
            }
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void testGetNthAddressIPv6(){ // Testing 2 bytes worth of IP-address combinations.
        for(int i = 0; i < (1 << 16); i++){
            try{
                String ip = "fd00:dead:beef:cafe::" + Integer.toHexString(i);
                assertEquals(v6.getNthAddress(BigInteger.valueOf(i)), getAddress(ip));
            }catch (AssertionError ae){
                Assert.fail();
            }
        }
    }

    @Test
    public void testGetFirstAddressIPv6(){ // Tests both methods of get IPv4 Nth address.
        try{
            Inet6Address target = (Inet6Address) InetAddress.getByName("fd00::1");
            for(int i = 8; i < 128; i++){
                Inet6Subnet test = Inet6Subnet.of("fd00::/" + i);
                assertEquals(target, test.getFirstAddress());
            }
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void testGetLastAddressIPv6(){
        try{
            Inet6Address[] ips = {
                    (Inet6Address) InetAddress.getByName("fdff:ffff:ffff:ffff:ffff:ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd0f:ffff:ffff:ffff:ffff:ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00:ffff:ffff:ffff:ffff:ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00:0fff:ffff:ffff:ffff:ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00:00ff:ffff:ffff:ffff:ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00:000f:ffff:ffff:ffff:ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::ffff:ffff:ffff:ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::fff:ffff:ffff:ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::ff:ffff:ffff:ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::f:ffff:ffff:ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::ffff:ffff:ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::fff:ffff:ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::ff:ffff:ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::f:ffff:ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::ffff:ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::fff:ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::ff:ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::f:ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::ffff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::fff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::ff:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::f:ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::ffff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::fff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::ff:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::f:ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::ffff"),
                    (Inet6Address) InetAddress.getByName("fd00::fff"),
                    (Inet6Address) InetAddress.getByName("fd00::ff"),
                    (Inet6Address) InetAddress.getByName("fd00::f"),
                    (Inet6Address) InetAddress.getByName("fd00::")
            };
            for(int i = 0; i < ips.length; i++){
                Inet6Subnet test = Inet6Subnet.of("fd00::/" + (4 * i + 8));
                assertEquals(ips[i], test.getLastAddress());
            }
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void testGetRandomAddress(){ // Given that function 'contains()' is 100% correct.
        try{
            for(int i = 0; i < (1 << 20); i++){
                assertTrue(v6.contains(v6.getRandomAddress()));
            }
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void testSummarizeIPv6(){
        try{

            // Subnets are equal.

            Inet6Subnet a = Inet6Subnet.of("fd00::/64");
            Inet6Subnet b = Inet6Subnet.of("fd00::/64");
            assertEquals(a, InetSubnet.summarize(a,b));

            // Adjecent continous subnet summary

            a = Inet6Subnet.of("fd00::1:0/112");
            b = Inet6Subnet.of("fd00::0:0/112");
            Inet6Subnet c = Inet6Subnet.of("fd00::0:0/111");
            assertEquals(c, InetSubnet.summarize(a,b));

            // Subnet a contains entire subnet b

            a = Inet6Subnet.of("fd00::/64");
            b = Inet6Subnet.of("fd00::/65");
            assertEquals(a, InetSubnet.summarize(a,b));

            a = Inet6Subnet.of("fd00::/64");
            b = Inet6Subnet.of("fd00::1:0:0:0/65");
            assertEquals(a, InetSubnet.summarize(a,b));

        }catch (Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    private InetAddress getAddress(String ip){
        try{
            return InetAddress.getByName(ip);
        }catch (Exception e){
            System.exit(-1);
            return null;
        }
    }
}
