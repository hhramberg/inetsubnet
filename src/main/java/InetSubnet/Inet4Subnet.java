package InetSubnet;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.nio.ByteBuffer;

public class Inet4Subnet extends InetSubnet{

    /**
     * Creates a new InetSubnet.Inet4Subnet object. Because this constructor is private and called by InetSubnet.Inet4Subnet.of() methods
     * new objects can be created very safely.
     * @param id Inet4Address representing this subnets subnet id.
     * @param mask Integer value representing this subnets network mask in CIDR notation.
     */
    Inet4Subnet(Inet4Address id, int mask) {
        super(id, mask);
    }

    /**
     * Instanciates a new InetSubnet.Inet4Subnet.
     * @param subnetId Inet4Address representing the subnet ID of the subnet.
     * @param mask Integer value of the network mask. Values between 0 and 32 are required.
     * @return InetSubnet.Inet4Subnet representing the specified subnet ID and network mask. If either argument is considered invalid, null is returned.
     */
    public static Inet4Subnet of(Inet4Address subnetId, int mask){
        return (Inet4Subnet) InetSubnet.of(subnetId, mask);
    }

    /**
     * Instanciates a new InetSubnet.Inet4Subnet.
     * @param subnetId Inet4Address representing the subnet ID of the subnet.
     * @param mask Integer value of the network mask. Values between 0 and 32 are required.
     * @param owner An object to associate with this subnet. The owner argument can be null.
     * @return InetSubnet.Inet4Subnet representing the specified subnet ID and network mask. If either argument is considered invalid, null is returned.
     */
    public static Inet4Subnet of(Inet4Address subnetId, int mask, Object owner){
        Inet4Subnet net = (Inet4Subnet) InetSubnet.of(subnetId, mask);
        if(net != null)
            net.setOwner(owner);
        return net;
    }

    /**
     * Instanciates a new InetSubnet.Inet4Subnet given a CIDR style IP address and network mask combination. For example: 10.0.0.0/24
     * @param prefixedAddress String representing a CIDR styled IPv4 IP address.
     * @return InetSubnet.Inet4Subnet representing the specified subnet ID and network mask. If the prefixedAddress argument is considered invalid, null is returned.
     */
    public static Inet4Subnet of(String prefixedAddress){
        return (Inet4Subnet) InetSubnet.of(prefixedAddress);
    }

    /**
     * Gets this subnet's subnet id.
     * @return Subnet id as an Inet4Address object.
     */
    @Override
    public Inet4Address getSubnetId(){
        return (Inet4Address)super.getSubnetId();
    }

    /**
     * A stripped down version of the general InetSubnet.InetSubnet.getNthAddress(), the InetSubnet.Inet4Subnet.getNthAddress() provides the
     * same feature as the general, but with normal Java integer data type argument. IPv4 has 32-bit address space, so there is
     * no requirement to use BigInteger.
     * @param n The numbered address to get.
     * @return InetAddress which is the Nth in this particular subnet, if addresses are sorted from lowest to highest.
     * @throws IndexOutOfBoundsException if requesting to get an address that isn't within the scope of this subnet.
     */
    public Inet4Address getNthAddress(int n) throws IndexOutOfBoundsException{
        if(n >= 0 && n < (1 << (32 - super.getMaskCidr()))){
            try{
                byte[] subnetId = this.getSubnetId().getAddress();
                int length = subnetId.length;
                byte[] N = ByteBuffer.allocate(4).putInt(n).array();
                byte[] val = new byte[length];

                int[] a = new int[length];
                int[] b = new int[length];
                int[] delta = new int[length];
                int save = 0;

                for(int i = length - 1; i >= 0; i--){
                    a[i] = Byte.toUnsignedInt(subnetId[i]);
                    b[i] = Byte.toUnsignedInt(N[i]);
                    delta[i] = a[i] + b[i] + save;
                    if(delta[i] > 255){
                        save = 1;
                        delta[i] = 0;
                    }else{
                        save = 0;
                    }
                    val[i] = (byte)delta[i];
                }
                return (Inet4Address) InetAddress.getByAddress(val);
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }else{
            throw new IndexOutOfBoundsException("Address number " + n + " is outside the scope of this subnet.");
        }
    }

    /**
     *
     * @return IPv4 private address scope Class A.
     */
    public static Inet4Subnet getPrivateScopeA(){
        return Inet4Subnet.of("10.0.0.0/8");
    }

    /**
     *
     * @return IPv4 private address scope Class B.
     */
    public static Inet4Subnet getPrivateScopeB(){
        return Inet4Subnet.of("172.16.0.0/12");
    }

    /**
     *
     * @return IPv4 private address scope Class C.
     */
    public static Inet4Subnet getPrivateScopeC(){
        return Inet4Subnet.of("192.168.0.0/16");
    }
}
