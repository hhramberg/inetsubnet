package InetSubnet;

import javax.naming.SizeLimitExceededException;
import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Random;


/**
 * <p>
 * Represents an IP-addressable network scope to be represented as a combination of IP address and network mask.
 * Both IPv4 32-bit and IPv6 128-bit addresses are supported.
 * InetSubnet enables some basic functions for getting IP addresses within a specified subnet, finding the subnets boundaries, and summarizing subnets.
 * </p>
 *
 * <p>
 * IP subnets can be represented in either CIDR notation (10.0.0.0/24 and fd00:dead:beef:cafe::/64)
 * or dotted decimal (IPv4)/ colon separated hexadecimal (IPv6, not common) (255.255.255.0 and ffff:ffff:ffff:ffff::).
 * </p>
 */

public class InetSubnet implements Comparable<InetSubnet> {
    public final static int IPV4_BITS = 32;
    public final static int IPV6_BITS = 128;
    private final InetAddress id;
    private final int mask;
    private Object owner;

    /**
     * Creates a new InetSubnet instance given InetAddress and CIDR mask.
     * @param id Subnet ID of this subnet.
     * @param mask Subnet mask of this subnet in CIDR format.
     */
    InetSubnet(InetAddress id, int mask)  {
        this.id = id;
        this.mask = mask;
    }

    /**
     * Creates a new ambiguous InetSubnet instance given InetAddress and CIDR mask.
     * @param prefixedAddress String with CIDR notation IP address and network mask.
     * @return InetSubnet represented by the prefixedAddress or null if it isn't a valid IP subnet.
     */
    public static InetSubnet of(String prefixedAddress){
        if(prefixedAddress == null || !prefixedAddress.contains("/"))
            return null;
        String[] split = prefixedAddress.trim().split("/");
        int mask;
        InetAddress id;

        try{
            mask = Integer.parseInt(split[1]);
            id = InetAddress.getByName(split[0]);
        }catch (Exception e){
            return null;
        }

        return of(id, mask);
    }

    /**
     * Creates a new InetSubnet instance given the InetAddress and CIDR mask.
     * @param id InetAddress of an address within the subnet.
     * @param mask CIDR mask defining the size of the subnet.
     * @return InetSubnet of this input parameters, or null if something went wrong.
     */
    public static InetSubnet of(InetAddress id, int mask){
        if(id == null || mask < 8 || mask > IPV6_BITS || (id.getAddress().length == 4 && mask > IPV4_BITS)) {
            return null;
        }else{
            try{
                if(id.getAddress().length == 4 && mask <= IPV4_BITS)
                    return new Inet4Subnet((Inet4Address) getSubnetId(id.getAddress(), mask), mask);
                else
                    return new Inet6Subnet((Inet6Address) getSubnetId(id.getAddress(), mask), mask);
            }catch (UnknownHostException uhe){
                return null;
            }
        }
    }

    /**
     * Return subnet mask as an integer in CIDR format.
     * @return the integer value of this subnets subnet mask in CIDR format.
     */
    public int getMaskCidr(){
        return this.mask;
    }

    /**
     * Return subnet mask in an InetAddress object.
     * @return This subnet's mask encapsulated in an InetAddress object.
     */
    public InetAddress getMask(){
        byte[] mask = new byte[this.id.getAddress().length];
        int m = this.mask;

        for(int i = 0; i < this.id.getAddress().length; i++){
            if(m >= 8){
                mask[i] = (byte)0xFF;
                m -= 8;
            }else{
                mask[i] = (byte)(0xFF << (8 - m));
                break;
            }
        }
        try{
            return InetAddress.getByAddress(mask);
        }catch (UnknownHostException uhe){
            return null;
        }
    }

    /**
     * Returns this subents subnet id.
     * @return this subnets id address as an InetAddress object.
     */
    public InetAddress getSubnetId(){
        return this.id;
    }

    /**
     * Returns this subnets broadcast address.
     * @return InetAddress representation of this subnet's broadcast address.
     */
    public InetAddress getBroadcast(){
        int l = this.id.getAddress().length;
        byte[] res = new byte[l];
        byte[] mask = this.getMask().getAddress();
        for(int i = 0; i < l; i++){
            res[i] = (byte) (this.id.getAddress()[i] + ~mask[i]);
        }
        try{
            return InetAddress.getByAddress(res);
        }catch (UnknownHostException uhe){
            return null;
        }
    }

    /**
     * Generates a randomly chosen InetAddress within this particular subnet.
     * @return Randomly generated InetAddress within the scope of this subnet.
     */
    public InetAddress getRandomAddress(){
        int space;
        if(this.id instanceof Inet6Address){
            space = IPV6_BITS;
        }else{
            space = IPV4_BITS;
        }
        Random rand = new Random();
        BigInteger n = new BigInteger(space - this.mask, rand); // Could be something wrong with this, because possible addresses are 2^n - 1, not 2^n
        return getNthAddress(n);
    }

    /**
     * Returns the first usable address, that is the first address after the subnet id.
     * For /31 networks (IPv4) and /127 networks (IPv6) the broadcast / last address wil be returned.
     * For /32 networks (IPv4) and /128 networks (IPv6) it will fail and return null, because there are no addresses
     * available after the subnet id.
     * @return The first usable InetAddress in this subnet.
     */
    public InetAddress getFirstAddress(){
        return getNthAddress(BigInteger.ONE);
    }

    /**
     * Returns the last usable address (IPv6) and broadcast (IPv4).
     * For /32 networks (IPv4) and /128 networks (IPv6) it will fail and return null.
     * @return InetAddress that is the last/broadcast address of this subnet.
     */
    public InetAddress getLastAddress(){
        int space;
        if(this.id instanceof Inet6Address){
            space = IPV6_BITS;
        }else{
            space = IPV4_BITS;
        }
        return getNthAddress(BigInteger.ONE.shiftLeft(space - this.mask).subtract(BigInteger.ONE));
    }

    /**
     * Returns a specific numbered address from this particular subnet.
     * @param n The numbered address to get.
     * @return InetAddress which is the Nth in this particular subnet, if addresses are sorted from lowest to highest.
     * @throws IndexOutOfBoundsException if requesting to get an address that isn't within the scope of this subnet.
     */
    public InetAddress getNthAddress(BigInteger n) throws IllegalArgumentException {
        if(n.compareTo(this.getNumberOfAddresses()) <= 0 && n.compareTo(BigInteger.ZERO) >= 0){
            try{
                byte[] id = this.id.getAddress();
                byte[] N = bigIntegerToByteArray(n, id.length);
                byte[] res = new byte[id.length];
                int[] delta = new int[id.length];
                int save = 0;

                for(int i = id.length - 1; i >= 0; i--){
                    delta[i] = Byte.toUnsignedInt(id[i]) + Byte.toUnsignedInt(N[i]) + save;
                    if(delta[i] > 255){
                        save = 1;
                        delta[i] = 0;
                    }else{
                        save = 0;
                    }
                    res[i] = (byte)delta[i];
                }
                return InetAddress.getByAddress(res);
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }else{
            throw new IllegalArgumentException("Address number " + n.toString() + " is outside the scope of this subnet.");
        }
    }

    /**
     * <p>
     * Tries to summarize the two input argument subnets provided, best effort. There are four possible prerequisites for subnet summarization.
     * Only one of the three are needed to successfully summarize the input subnets sub1 and sub2
     * </p>
     * <div>
     * <ol>
     * <li>Both subnets are the same.</li>
     * <li>Both subnets share a common subnet ID, but have different masks.</li>
     * <li>Subnet IDs differ but one subnet contains the entire address space of the other.</li>
     * <li>Both subnets share masks, and one subnet's subnet ID is the next numerical byte value IP-address after the other's broadcast/last address.
     * Phrased differently: both subnets form a continuous larger single subnet.</li>
     * </ol>
     * </div>
     *
     * <p>
     * For example: Given a = 10.0.0.0/24 and b = 10.0.1.0/24
     * a and b share mask, and b's subnet ID is the next numerical address after a's broadcast address.
     * The result will be 10.0.0.0/23
     * </p>
     *
     * <p>
     * Another example: Given a = 10.0.0.0/24 and b = 10.0.0.128/25
     * a contains b entirely, so there is no need to save b for itself, if you want to save space.
     * The result will be 10.0.0.0/24
     * </p>
     *
     * @param sub1 First subnet to summarize.
     * @param sub2 Second subnet to summarize.
     * @return Summarized InetSubnet if possible. Null if summary was unsuccessful.
     */
    public static InetSubnet summarize(InetSubnet sub1, InetSubnet sub2){
        try{
            if(sub1.getSubnetId().getAddress().length == sub2.getSubnetId().getAddress().length){

                // Check for equality. If both subnets are the same, just return either.

                if(sub1.equals(sub2)){
                    return sub1;
                }

                // Check for same network overlap with same subnet id: 10.0.0.0/24 contains 10.0.0.0/25

                if(sub1.getSubnetId().equals(sub2.getSubnetId())){
                    if(sub1.getMaskCidr() < sub2.getMaskCidr()){
                        return sub1;
                    }else{
                        return sub2;
                    }
                }

                // Check for same network overlap, without same subnet id: 10.0.0.0/24 contains 10.0.0.128/25

                if(sub1.getMaskCidr() < sub2.getMaskCidr() && sub1.contains(sub2.getSubnetId())){
                    return sub1;
                }else if(sub2.getMaskCidr() < sub1.getMaskCidr() && sub2.contains(sub1.getSubnetId())){
                    return sub2;
                }

                // Check for adjecent network overlap, 10.0.0.0/24 + 10.0.1.0/24 = 10.0.0.0/23

                if(sub1.getMaskCidr() == sub2.getMaskCidr()){
                    if(InetSubnet.getNextAddress(sub1.getLastAddress()).equals(sub2.getSubnetId())){
                        if(sub1.getSubnetId().getAddress().length > 4){
                            return Inet6Subnet.of((Inet6Address)sub1.getSubnetId(), sub1.getMaskCidr() - 1);
                        }else{
                            return Inet4Subnet.of((Inet4Address)sub1.getSubnetId(), sub1.getMaskCidr() - 1);
                        }
                    }else if(InetSubnet.getNextAddress(sub2.getLastAddress()).equals(sub1.getSubnetId())){
                        if(sub1.getSubnetId().getAddress().length > 4){
                            return Inet6Subnet.of((Inet6Address)sub2.getSubnetId(), sub2.getMaskCidr() - 1);
                        }else{
                            return Inet4Subnet.of((Inet4Address)sub2.getSubnetId(), sub2.getMaskCidr() - 1);
                        }
                    }
                }

                // No checks panned out -> no summary possible.

                return null;
            }else{
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Checks whether the input InetAddress is part of this subnet.
     * @param address InetAddress to check for affiliation.
     * @return True if this subnet contains the input address, false if not.
     */
    public boolean contains(InetAddress address) {
        if(
            address != null &&
            this.id.getAddress().length == address.getAddress().length
        ){
            byte[] subnetId = this.id.getAddress();
            byte[] addressToCheck = address.getAddress();
            byte[] mask = this.getMask().getAddress();
            for(int i = 0; i < subnetId.length; i++){
                if(subnetId[i] != (byte)(addressToCheck[i] & mask[i])){
                    return false;
                }
            }
            return true;
        }else{
            return false;
        }
    }

    /**
     * Checks whether the input String is an InetAddress and whether or not this address is part of this subnet.
     * @param address String to convert to IP-address and check for affiliation.
     * @return True if this subnet contains the input address, false if not.
     */
    public boolean contains(String address) {
        InetAddress ip;
        try{
            ip = InetAddress.getByName(address);
            return contains(ip);
        }catch (Exception e){
            return false;
        }
    }

    /**
     * Gets the Object-variable of this subnet.
     * @return Object variable associated with this subnet.
     */
    public Object getOwner(){
        return this.owner;
    }

    /**
     * Sets the Object variable to be associated with this subnet.
     * @param newOwner Object to associate with this subnet.
     */
    public void setOwner(Object newOwner){
        this.owner = newOwner;
    }

    /**
     * Finds the number of addresses defined in this subnet.
     * BigInteger is required to address the issue that is IPv6 128-bit address space.
     * The number of possible address combinations far exceed the capabilities of any trivial Java integer data type.
     * @return Number of addresses defined in this subnet as a BigInteger.
     */
    public BigInteger getNumberOfAddresses(){
        if(this.id instanceof Inet6Address){
            return BigInteger.ONE.shiftLeft(IPV6_BITS - this.mask).subtract(BigInteger.ONE);
        }else{
            return BigInteger.ONE.shiftLeft(IPV4_BITS - this.mask).subtract(BigInteger.ONE);
        }
    }

    /**
     * Returns a byte array represented by the input argument BigInteger.
     * @param n BigInteger to convert.
     * @param length Length of the returned byte array.
     * @return Byte array of specified BigInteger with given length.
     * @throws SizeLimitExceededException if the BigIntegers byte arrays length is larger than specified length parameter.
     */
    private static byte[] bigIntegerToByteArray(BigInteger n, int length) throws SizeLimitExceededException {
        byte[] val = new byte[length];
        byte[] N = n.toByteArray();
        if(N.length > length){
            throw new SizeLimitExceededException("BigInteger number requires more bytes than specified by length argument.");
        }else{
            System.arraycopy(N, 0, val, length - N.length, N.length);
        }
        return val;
    }

    /**
     *
     * @param address IP address byte array to assign as subnet id.
     * @param mask CIDR mask that defines the subnet size.
     * @return InetAddress representing the subnet id of this subnet.
     * @throws UnknownHostException if the address byte array doesn't specify an IPv4 or IPv6 address.
     */
    static InetAddress getSubnetId(byte[] address, int mask) throws UnknownHostException {
        byte[] res = new byte[address.length];
        int m = mask;
        for(int i = 0; i < res.length; i++){
            if(m >= 8){
                res[i] = address[i];
                m -= 8;
            }else{
                res[i] = (byte) (address[i] & (0xFF << (8 - m)));
                break;
            }
        }
        return InetAddress.getByAddress(res);
    }

    /**
     * Finds the next numerical address after the input argument address.
     * @param ip Basis address of which to calculate the next numerical value address.
     * @return InetAddress that is the next IP-address after the input argument.
     */
    public static InetAddress getNextAddress(InetAddress ip){
        try{
            byte[] ipbytes = ip.getAddress();

            int length = ipbytes.length;
            byte[] N = bigIntegerToByteArray(BigInteger.ONE, length);
            byte[] val = new byte[length];

            int[] a = new int[length];
            int[] b = new int[length];
            int[] delta = new int[length];
            int save = 0;

            for(int i = length - 1; i >= 0; i--){
                a[i] = Byte.toUnsignedInt(ipbytes[i]);
                b[i] = Byte.toUnsignedInt(N[i]);
                delta[i] = a[i] + b[i] + save;
                if(delta[i] > 255){
                    save = 1;
                    delta[i] = 0;
                }else{
                    save = 0;
                }
                val[i] = (byte)delta[i];
            }
            return InetAddress.getByAddress(val);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * Creates a short String object of this subnet with CIDR suffix.
     * @return Short descriptive String object of this subnet.
     */
    @Override
    public String toString(){
        return this.id.getHostAddress() + "/" + this.mask;
    }

    /**
     * <p>Enables sort()-functions to properly sort InetSubnets based on their numerical byte-values.
     * Lower byte value addresses take precedence over larger ones. For example:</p>
     * <p>10.0.0.0/24 will be sorted in front of 136.0.45.0/24</p>
     * <p>If subnet IDs match, mask break the tie. Having a larger value mask take precedence over lower value masks. For example:</p>
     * <p>10.0.0.0/24 will be sorted in front of 10.0.0.0/25 because its mask is larger.</p>
     * @param obj InetSubnet object to compare to this specific subnet.
     * @return Integer whose value will differentiate this specific subnet to the compared input argument subnet.
     */
    public int compareTo(InetSubnet obj) {
//        if(this.id.getAddress().length > obj.id.getAddress().length)
//            return 1;
        try{
            byte[] a = this.id.getAddress();
            byte[] b = obj.getSubnetId().getAddress();
            int[] c = new int[a.length];
            int[] d = new int[b.length];
            for(int i = 0; i < a.length; i++){
                c[i] = Byte.toUnsignedInt(a[i]);
                d[i] = Byte.toUnsignedInt(b[i]);
                if(c[i] != d[i])
                    return c[i] - d[i];
            }
        }catch (ArrayIndexOutOfBoundsException e){
            return 0;
        }
        return this.getMaskCidr() - obj.getMaskCidr();
    }

    /**
     * Enables two InetSubnets to be compared to check for equality.
     * Only the subnet ID and mask, as well as object type, have to match.
     * @param obj Object of which this subnet is to be compared.
     * @return True if both subnets have the same subnet ID and mask. False if this is not the case.
     */
    @Override
    public boolean equals(Object obj){
        return(obj instanceof InetSubnet
                && this.id.equals(((InetSubnet)obj).id)
                && this.mask == ((InetSubnet)obj).mask);
    }
}