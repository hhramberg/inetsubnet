package InetSubnet;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Inet6Subnet extends InetSubnet {

    /**
     * Creates a new InetSubnet.Inet6Subnet object. Because this constructor is private and called by InetSubnet.Inet6Subnet.of() methods
     * new objects can be created very safely.
     * @param subnetId Inet6Address representing this subnets subnet id.
     * @param mask Integer value representing this subnets network mask in CIDR notation.
     */
    Inet6Subnet(Inet6Address subnetId, int mask) {
        super(subnetId, mask);
    }

    /**
     * Instanciates a new InetSubnet.Inet6Subnet.
     * @param subnetId InetAddress representing the subnet ID of the subnet.
     * @param mask Integer value of the network mask. Values between 0 and 128 are required.
     * @return InetSubnet.Inet6Subnet representing the specified subnet ID and network mask. If either argument is considered invalid, null is returned.
     */
    public static Inet6Subnet of(Inet6Address subnetId, int mask){
        try{
            if(mask > 8 && mask <= IPV6_BITS)
                return new Inet6Subnet((Inet6Address) InetSubnet.getSubnetId(subnetId.getAddress(), mask), mask);
            else
                return null;
        }catch (UnknownHostException uhe){
            return null;
        }
    }

    /**
     * Instanciates a new InetSubnet.Inet6Subnet.
     * @param subnetId InetAddress representing the subnet ID of the subnet.
     * @param mask Integer value of the network mask. Values between 0 and 128 are required.
     * @param owner An object to associate with this subnet. The owner argument can be null.
     * @return InetSubnet.Inet6Subnet representing the specified subnet ID and network mask. If either argument is considered invalid, null is returned.
     */
    public static Inet6Subnet of(Inet6Address subnetId, int mask, Object owner){
        Inet6Subnet net = Inet6Subnet.of(subnetId, mask);
        if(net != null)
            net.setOwner(owner);
        return net;
    }

    /**
     * Instanciates a new InetSubnet.Inet6Subnet given a CIDR style IP address and network mask combination. For example: fd00::/8
     * @param prefixedAddress String representing a CIDR styled IPv6 IP address.
     * @return InetSubnet.Inet6Subnet representing the specified subnet ID and network mask. If the prefixedAddress argument is considered invalid, null is returned.
     */
    public static Inet6Subnet of(String prefixedAddress){
        return (Inet6Subnet) InetSubnet.of(prefixedAddress);
    }

    /**
     * Gets this subnet's subnet id.
     * @return Subnet id as an Inet6Address object.
     */
    @Override
    public Inet6Address getSubnetId(){
        return (Inet6Address)super.getSubnetId();
    }

    /**
     * Simple preconfigured IPv6 subnet.
     * @return The private IPv6 address scope.
     */
    public static Inet6Subnet getPrivateScope(){
        return Inet6Subnet.of("fc00::/7");
    }
}
