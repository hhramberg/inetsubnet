# InetSubnet

Java package for basic IPv4 and IPv6 subnet tasks. 

## About InetSubnet

Generate subnets using InetAddresses (Inet4Address or Inet6Address) and a CIDR notation subnet mask length. 
Provides features like getting the Nth address of a given subnet, summarizing two subnets into one if possible and checking 
whether or not an address is part of a given subnet.

InetSubnet offers Inet4Subnet and Inet6Subnet, for IPv4 and IPv6 networks respectively. Inet4Subnet and Inet6Subnet extends
InetSubnet, so you'll have to cast when necessary.

## About IP Subnets
IP networks consist of ranges of IP addresses. A network is often referred to as a subnet. 
That is, all the networks around the world are smaller networks of all the possible combinations of 
IPv4s 2^32 addresses and IPv6s 2^128 addresses.
A subnet is defined by its subnet id, the first IP-address in its range, and a network mask. 
For IPv4 the dotted decimal notation is popularly used, 
because each of its 4 bytes can be described using an unsigned 8-bit number (0-255).
An example IPv4 subnet with 512 addresses could be:

192.168.0.0 255.255.254.0,

which covers, or contains, all the addresses ranging from (inclusive) 192.168.0.0 to 192.168.1.255. 192.168.0.0 is the subnet id, 
and 255.255.254.0 is the network mask.

Another way of writing IPv4 addresses is the CIDR notation. The network mask is treated as an unsigned integer, 
where the integer value is equal to the network bits. 
For an IPv4 network there are 32 bits for any given address. If we 'lock' the 23 first leftmost bits, 
we have 9 remaining rightmost bits that define the addresses in that subnet.
A CIDR notation IPv4 address could be:

192.186.0.0/23 which is equivalent to 192.168.0.0 255.255.254.0

IPv6 uses CIDR notation exclusively. An example of an IPv6 subnet is:

fd00:0:0:0:0:0:0:0/64

which could be [truncated](https://www.ciscopress.com/articles/article.asp?p=2803866) to the following form: 

fd00::/64

If you remain unsure of what an IP subnet is, and how IPv4 and IPv6 subnets are described, 
I suggest you read up on both topics. I want to remain neutral in my reccommendations with regards to litterature, but any official Cisco or Juniper 'cert guide' literature would cover the very basic. University educational institutions should also be a good source for descriptins of IP subnets. You could also have a look at the [Wikipedia article](https://en.wikipedia.org/wiki/Subnetwork) on subnets, but I do reccommend playing with online IP subnet calcuators such as [this one](http://www.subnet-calculator.com/) to get familiar with IP address ranges and subnet masks.

The Owner object variable is available to associate _any_ data object with this InetSubnet object. As an application specific example you could associate an owner of the subnet with the InetSubnet object. Let's say Bob owns subnet 1.2.3.4/24, and any data directed towards this subnet should be considered as traffic directed to Bob's subnet.

## Installation
Clone project or download/copy required files as you see fit. Created using Java 1.8. There are no special prerequisites.

## How to use InetSubnet
Basic usage
```java
Inet4Subnet ip4 = Inet4Subnet.of("10.0.0.0/24");
Inet6Subnet ip6 = Inet6Subnet.of("fd00::/64");

Inet4Address broadcast = (Inet4Address) ip4.getLastAddress();
Inet6Address random = (Inet6Address) ip6.getRandomAddress();
Inet6Address someip = (Inet6Address) ip6.getNthAddress(BigInteger.valueOf(1234));
Inet6Address nextip = (Inet6Address) InetSubnet.getNextAddress(someip);

// Possible output
// 10.0.0.255
// fd00:0:0:0:d243:52e8:82cb:c3b5
// fd00:0:0:0:0:0:0:4d2
// fd00:0:0:0:0:0:0:4d3

```

Summarization
```java
Inet4Subnet a = Inet4Subnet.of("10.0.0.0/25");
Inet4Subnet b = Inet4Subnet.of("10.0.0.128/25");
Inet4Subnet summarized = Inet4Subnet.summarize(a, b);

System.out.println(a);
System.out.println(b);
System.out.println(summarized);

// Print to console:
// 10.0.0.0/25
// 10.0.0.128/25
// 10.0.0.0/24
```

## Method description

### public int getMaskCidr()
Returns the network mask in number bits of the given subnet.
```java
Inet4Subnet sales = Inet4Subnet.of("10.0.0.0/25");
System.out.println("Network bits in 'sales': " + sales.getMaskCidr());

// Network bits in 'sales': 25
```

### public InetAddress getMask()
Returns the network mask of the given subnet.
```java
Inet4Subnet sales = Inet4Subnet.of("10.0.0.0/25");
System.out.println("Network mask of 'sales': " + sales.getMask().getHostAddress());

// Network mask of 'sales': 255.255.255.128
```

### public InetAddress getSubnetId()
Returns the network ID of the given subnet.
```java
Inet4Subnet sales = Inet4Subnet.of("10.0.0.0/25");
System.out.println("Network ID of 'sales': " + sales.getSubnetId().getHostAddress());

// Network ID of 'sales': 10.0.0.0
```

### public InetAddress getBroadcast()
Returns the broadcast address of the given subnet.
```java
Inet4Subnet sales = Inet4Subnet.of("10.0.0.0/25");
System.out.println("Broadcast address of 'sales': " + sales.getBroadcast().getHostAddress());

// Broadcast address of 'sales': 10.0.0.127
```

### public InetAddress getRandomAddress()
Generates a random IP address in the given subnet.
```java
Inet4Subnet sales = Inet4Subnet.of("10.0.0.0/25");
System.out.println("Random IP in 'sales': " + sales.getRandomAddress());

// Possible output:
// Random IP in 'sales': 10.0.0.56
```

### public InetAddress getFirstAddress()
Returns the first usable IP address in the given subnet.
```java
Inet4Subnet sales = Inet4Subnet.of("10.0.0.0/25");
System.out.println("First IP in 'sales': " + sales.getFirstAddress());

// First IP in 'sales': 10.0.0.1
```

### public InetAddress getLastAddress()
Returns the last IP address in the given subnet. 
For IPv4 networks the given IP address will be equal of the subnet broadcast address. 
IPv6 does not utilize broadcast.
```java
Inet4Subnet sales = Inet4Subnet.of("10.0.0.0/25");
System.out.println("First IP in 'sales': " + sales.getLastAddress());

// Last IP in 'sales': 10.0.0.127
```

### public InetAddress getNthAddress(BigInteger n)
Returns the n'th address of the given subnet. If the input argument BigInteger n is smaller than 0 or larger than the
maximum supported addresses in the given subnet an IllegalArgumentException will be thrown.
```java
Inet4Subnet sales = Inet4Subnet.of("10.0.0.0/25");
System.out.println("Fifth IP in 'sales': " + sales.getNthAddress(BigInteger.valueOf(5)));

// Fifth IP in 'sales': 10.0.0.5
```

### public static InetSubnet summarize(InetSubnet sub1, InetSubnet sub2)
Tries to summarize two InetSubnets sub1 and sub2. Summary will take place of one of the three conditions are met:
* Both subnets are the same.
* Both subnets share a common subnet ID, but have different masks.
* Subnet IDs differ but one subnet contains the entire address space of the other.
* Both subnets share masks, and one subnet's subnet ID is the next numerical byte value IP-address 
after the other's broadcast/last address. Phrased differently: both subnets form a continuous larger single subnet.

If summary fails, null is returned.
```java
Inet4Subnet sales = Inet4Subnet.of("10.0.0.0/25");
Inet4Subnet production = Inet4Subnet.of("10.0.0.128/25");
System.out.println("Summary of 'sales' and 'production': " + InetSubnet.summarize(sales, production));

// Summary of 'sales' and 'production': 10.0.0.0/24
```

### public boolean contains(InetAddress address)
Checks whether or not a subnet contains the argument InetAddress.
```java
Inet4Subnet sales = Inet4Subnet.of("10.0.0.0/25");
System.out.println("'sales' contains 10.0.0.2: " + sales.contains((Inet4Address)InetAddress.getByName("10.0.0.2")));
System.out.println("'sales' contains 10.0.0.130: " + sales.contains((Inet4Address)InetAddress.getByName("10.0.0.130")));

// 'sales' contains 10.0.0.2: true
// 'sales' contains 10.0.0.130: false
```

### public boolean contains(String address)
Same as **boolean contains(InetAddress address)**, but creates the InetAddress object first.

### public BigInteger getNumberOfAddresses()
Returns the number of addresses in this subnet.
```java
Inet4Subnet sales = Inet4Subnet.of("10.0.0.0/25");
System.out.println("'sales' number of addresses: " + sales.getNumberOfAddresses());

// 'sales' number of addresses: 128
```

### public String toString()
Returns a descriptive String representation of this subnet.
```java
Inet4Subnet sales = Inet4Subnet.of("10.0.0.0", 25);
System.out.println(sales);

// 10.0.0.0/25
```

### public int compareTo(InetSubnet obj)
Compares the current InetSubnet with another InetSubnet. Lower byte value addresses will be sorted in front of higher ones.
```java
Inet4Subnet sales = Inet4Subnet.of("10.0.0.0/24");
Inet4Subnet production = Inet4Subnet.of("192.168.0.0/24");
System.out.println(sales.compareTo(production));

// -182
```

### public boolean equals(Object obj)
Checks the current InetSubnet with another object for equality. 
Subnets are equal if their subnet IDs and masks are equal.
```java
Inet4Subnet sales = Inet4Subnet.of("10.0.0.0/24");
Inet4Subnet production = Inet4Subnet.of("192.168.0.0/24");
System.out.println(sales.equal(production));

// false
```

## Tradeoffs
Some operations need BigInteger input arguments as a result of the 128-bit address-space of IPv6. 
For instance most IPv6 subnets are 64-bit, which exhaust even the largest Java integer datatype; long. 
Inet4Subnet provides an easier to use getNthAddress-function with int data type argument for IPv4 related tasks.